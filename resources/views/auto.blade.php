@extends('layouts.base')
@section('section')
  <!--Page Title-->
  <section class="page-title parallax" style="background-image:url(images/background/13.jpg);">
    <div class="auto-container">
      <h1>Auto e Frota</h1>
    </div>
  </section>
  <!--End Page Title-->
  <!-- About Us -->
  <section class="about-section-two alternate">
    <div class="auto-container">
      <div class="row clearfix">
        <!-- Content Column -->
        <div class="content-column col-lg-12 col-md-12 col-sm-12">
          <div class="inner-column">
            <span class="title text-center">Temos parceria com várias seguradoras, realize uma cotação conosco e confira e melhor opção para a sua necessidade.</span>
            <div class="row">
              <div class="col-lg-6 auto">
                <h4>Básicas(CASCOS):</h4>
                <ul>
                  <li>Colisão.</li>
                  <li>Incêndio.</li>
                  <li>Roubo e furto.</li>
                </ul>
                <br>
                <h4>Responsabilidade Civil – Danos causados a Terceiros:</h4>
                <ul>
                  <li>Danos Materiais.</li>
                  <li>Danos Corporais.</li>
                  <li>Danos Morais.</li>
                </ul>
                <br>
                <h4>Básicas(CASCOS):</h4>
                <ul>
                  <li>Colisão.</li>
                  <li>Incêndio.</li>
                  <li>Roubo e furto.</li>
                </ul>
                <br>
                <h4>Acidentes Pessoais Passageiros:</h4>
                <ul>
                  <li>Morte, Invalidez permanente, DMH.</li>
                </ul>
                <br>
                <h4>Contratação:</h4>
                <ul>
                  <li>Valor de Mercado – Tabela Fipe até 110%.</li>
                  <li>Valor Determinado (fixo até o Final da Vigência).</li>
                </ul>
                <br>
                <h4>Franquias:</h4>
                <p>Oferecemos, também, opções de franquia sob medida, conforme risco do Segurado.</p>
                <ul>
                  <li>Franquia Básica Normal.</li>
                  <li>Franquia Facultativa, com diversas opções, que reduz o custo do seguro.</li>
                </ul>
                <br>
                <h4>Adicionais disponíveis:</h4>
                <ul>
                  <li>Cobertura básica de vidros.</li>
                  <li>Cobertura completa de vidros (Retrovisores, Lanternas e Faróis).</li>
                  <li>Assistência 24 horas extensivo a residência.</li>
                  <li>Decessos (Assistência Funeral).</li>
                  <li>Indenização por imobilização.</li>
                  <li>Reembolso de despesas extras.</li>
                  <li>Extensão de garantia por 180 dias ou 12 meses para veículos novos (0 km).</li>
                  <li>Carro reserva (várias opções).</li>
                  <li>Acessórios de som e imagem.</li>
                  <li>Blindagem.</li>
                  <li>Kit a gás.</li>
                  <li>Equipamentos.</li>
                </ul>
                <br>
                <h4>Liquidação de Sinistros:</h4>
                <ul>
                  <li>Acompanhamento dos reparos do veículo até a data da liberação e saída da oficina.</li>
                  <li>Total assistência até a liquidação do sinistro.</li>
                </ul>
                <br>
                <h4>Além disso, podemos ainda oferecer coberturas adicionais, tais como:</h4>
                <ul>
                  <li>Assistência básica em viagem gratuita.</li>
                  <li>Perfil sob medida.</li>
                  <li>Responsabilidade civil - Objetos transportados gratuitos.</li>
                  <li>Serviços gratuitos e descontos em redes automotivas, estacionamentos, locação de veículos e muito mais.</li>
                </ul>
                <br>
                <p>Essas são apenas algumas informações sobre o nossa prestação de serviços. Para mais detalhes entre em contato com a nossa Central de Atendimento, ou se preferir, envie questionário do perfil devidamente preenchido.</p>
              </div>
              <div class="col-lg-6">
                <h4>Cote seu Seguro Auto e Receba a Melhor Proposta</h4>
                <p class="text-center">Preencha o formulário abaixo</p>
                <form class="cotacaoForm" id="cotacaoForm">
                  {{ csrf_field() }}
                  <input type="hidden" value="{{date('d/m/Y')}}" name="data">
                  <div class="row clearfix">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="nome" placeholder="Nome Completo" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="telefone" placeholder="Telefone" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="email" name="email" placeholder="Email" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="tipo_seguro" name="tipo_seguro">
                        <option selected="true" disabled="disabled">Tipo de Seguro*</option>
                        <option value="Novo">Novo</option>
                        <option value="Renovação">Renovação</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="seg_atual" placeholder="Seguradora Atual" >
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="class_atual" placeholder="Classe de Bonus Atual" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="venc_apolice_atual" placeholder="Vecimento da Apolice Atual" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="numero_apolice" placeholder="Numero da Apolice" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="modelo_veiculo" placeholder="Modelo do Veiculo" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="ano_veiculo" placeholder="Ano" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="chassis_veiculo" placeholder="Chassis" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="placa_veiculo" placeholder="Placa" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="assunto" name="assunto">
                        <option selected="true" disabled="disabled">Assunto</option>
                        <option value="Sim">Sim</option>
                        <option value="Não">Não</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="principal_condutor" placeholder="Nome do Principal Condutor" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="cpf_condutor" placeholder="CPF" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="cnh_condutor" placeholder="CNH" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="data_nascimento_condutor" placeholder="Data De Nascimento" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="estado_condutor" name="estado_condutor">
                        <option selected="true" disabled="disabled">Estado Civil Principal Condutor</option>
                        <option value="Casado">Casado</option>
                        <option value="Solteiro">Solteiro</option>
                        <option value="Viuvo">Viuvo</option>
                        <option value="Divorciado">Divorciado</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="outros_condutores" name="outros_condutores">
                        <option selected="true" disabled="disabled">Existe Outros Condutores?</option>
                        <option value="Não Existe">Não Existe</option>
                        <option value="de 18 a 25 anos">de 18 a 25 anos</option>
                        <option value="Acima de 26 anos">Acima de 26 anos</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="profissao_condutor" placeholder="Profissão" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="uso_veiculo" name="uso_veiculo">
                        <option selected="true" disabled="disabled">Uso do Veiculo</option>
                        <option value="Apenas Lazer">Apenas Lazer</option>
                        <option value="Ida e Volta Trabalho">Ida e Volta Trabalho</option>
                        <option value="Faculdade">Faculdade</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="garagem" name="garagem">
                        <option selected="true" disabled="disabled">Garagem</option>
                        <option value="Residencia">Residencia</option>
                        <option value="Trabalho">Trabalho</option>
                        <option value="Faculdade">Faculdade</option>
                        <option value="Nenhum">Nenhum</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="tipo_residencia" name="tipo_residencia">
                        <option selected="true" disabled="disabled">Tipo de Residencia</option>
                        <option value="Casa/Sobrado">Casa/Sobrado</option>
                        <option value="Apartamento">Apartamento</option>
                        <option value="Casa em condominio fechado">Casa em condominio fechado</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="endereco" placeholder="Endereço e complemento" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <input class="form-control" type="text" name="cep" placeholder="Cep de Pernoite" required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <select class="form-control" id="tipo_portao" name="tipo_portao">
                        <option selected="true" disabled="disabled">Tipo de Portão</option>
                        <option value="Manual">Manual</option>
                        <option value="Automatico">Automatico</option>
                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <textarea class="form-control" name="obs" placeholder="Alguma Observação?" id="obs" rows="3"></textarea>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center">
                      <button class="theme-btn btn-style-three btnCotacao"  name="Submit">Enviar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End About Us -->

@endsection
