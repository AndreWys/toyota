<section class="call-to-action" style="background-image: url({{asset('images/background/para_voce.jpg')}});">
  <div class="auto-container">
    <div class="outer-box clearfix">
      <div class="title-column">
        <h3>Várias opções de seguros quem buscam segurança e tranquilidade.</h3>
      </div>

      <div class="btn-column">
        <div class="btn-box">
          <a href="contact.html" class="theme-btn btn-style-one">Saiba Mais</a>
        </div>
      </div>
    </div>
  </div>
</section>
