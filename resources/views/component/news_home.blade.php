<!-- News Section -->
<section class="news-section">
  <div class="auto-container">
    <div class="sec-title">
      <span class="title">Não Perca</span>
      <h2>Noticias Recentes</h2>
    </div>

    <div class="news-carousel owl-carousel owl-theme">
      <!-- News Block -->
      <div class="news-block">
        <div class="inner-box">
          <div class="image-box">
            <div class="image"><a href="noticias-detalhes"><img src="{{asset('images/news_cyber.jpg')}}" alt=""></a></div>
          </div>
          <div class="lower-content">
            <h3><a href="noticias-detalhes">Cyber Seguro, sua Empresa está protegida?</a></h3>
            <span class="date">Post: 15 Abril 2018</span>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut..</div>
            <div class="link-box"><a href="noticias-detalhes">Saiba Mais</a></div>
          </div>
        </div>
      </div>

      <!-- News Block -->
      <div class="news-block">
        <div class="inner-box">
          <div class="image-box">
            <div class="image"><a href="noticias-detalhes"><img src="{{asset('images/news_trans.jpg')}}" alt=""></a></div>
          </div>
          <div class="lower-content">
            <h3><a href="noticias-detalhes">Transportes: Gestão entre seguradoras e clientes</a></h3>
            <span class="date">Post: 15 Abril 2018</span>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut..</div>
            <div class="link-box"><a href="noticias-detalhes">Saiba Mais</a></div>
          </div>
        </div>
      </div>

      <!-- News Block -->
      <div class="news-block">
        <div class="inner-box">
          <div class="image-box">
            <div class="image"><a href="noticias-detalhes"><img src="{{asset('images/news_buss.jpg')}}" alt=""></a></div>
          </div>
          <div class="lower-content">
            <h3><a href="noticias-detalhes">Agronegócio: Soluções de gestão</a></h3>
            <span class="date">Post: 15 Abril 2018</span>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut..</div>
            <div class="link-box"><a href="noticias-detalhes">Saiba Mais</a></div>
          </div>
        </div>
      </div>

      <!-- News Block -->
      <div class="news-block">
        <div class="inner-box">
          <div class="image-box">
            <div class="image"><a href="noticias-detalhes"><img src="{{asset('images/news_cyber.jpg')}}" alt=""></a></div>
          </div>
          <div class="lower-content">
            <h3><a href="noticias-detalhes">Cyber Seguro, sua Empresa está protegida?</a></h3>
            <span class="date">Post: 15 Abril 2018</span>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut..</div>
            <div class="link-box"><a href="noticias-detalhes">Saiba Mais</a></div>
          </div>
        </div>
      </div>

      <!-- News Block -->
      <div class="news-block">
        <div class="inner-box">
          <div class="image-box">
            <div class="image"><a href="noticias-detalhes"><img src="{{asset('images/news_trans.jpg')}}" alt=""></a></div>
          </div>
          <div class="lower-content">
            <h3><a href="noticias-detalhes">Transportes: Gestão entre seguradoras e clientes</a></h3>
            <span class="date">Post: 15 Abril 2018</span>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut..</div>
            <div class="link-box"><a href="noticias-detalhes">Saiba Mais</a></div>
          </div>
        </div>
      </div>

      <!-- News Block -->
      <div class="news-block">
        <div class="inner-box">
          <div class="image-box">
            <div class="image"><a href="noticias-detalhes"><img src="{{asset('images/news_buss.jpg')}}" alt=""></a></div>
          </div>
          <div class="lower-content">
            <h3><a href="noticias-detalhes">Agronegócio: Soluções de gestão</a></h3>
            <span class="date">Post: 15 Abril 2018</span>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut..</div>
            <div class="link-box"><a href="noticias-detalhes">Saiba Mais</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- News Section -->
