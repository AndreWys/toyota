<section class="main-slider">
      <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
        <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
          <ul>
            <!-- Slide 1 -->
            <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">

              <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-1.jpg">

              <div class="tp-caption"
              data-paddingbottom="[0,0,0,0]"
              data-paddingleft="[0,0,0,0]"
              data-paddingright="[0,0,0,0]"
              data-paddingtop="[0,0,0,0]"
              data-responsive_offset="on"
              data-type="text"
              data-height="none"
              data-whitespace="nowrap"
              data-width="auto"
              data-hoffset="['0','15','15','15']"
              data-voffset="['-165','-165','-165','-165']"
              data-x="['left','left','left','left']"
              data-y="['middle','middle','middle','middle']"
              data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
              <h4>Toyota Tsusho</h4>
            </div>

            <div class="tp-caption"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingtop="[0,0,0,0]"
            data-responsive_offset="on"
            data-type="text"
            data-height="none"
            data-whitespace="nowrap"
            data-fontsize="['64','40','36','24']"
            data-width="auto"
            data-hoffset="['0','15','15','15']"
            data-voffset="['-75','-75','-85','-95']"
            data-x="['left','left','left','left']"
            data-y="['middle','middle','middle','middle']"
            data-frames='[{"delay":500,"split":"chars","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
            <h2>Cuidando do seu amanhã</h2>
          </div>

          <div class="tp-caption"
          data-paddingbottom="[0,0,0,0]"
          data-paddingleft="[0,0,0,0]"
          data-paddingright="[0,0,0,0]"
          data-paddingtop="[0,0,0,0]"
          data-responsive_offset="on"
          data-type="text"
          data-height="none"
          data-whitespace="normal"
          data-width="['600','800','750','550']"
          data-hoffset="['0','15','15','15']"
          data-voffset="['15','15','15','15']"
          data-x="['left','left','left','left']"
          data-y="['middle','middle','middle','middle']"
          data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
          <div class="text">A Toyota Tsusho Corretora de Seguros é uma empresa do Grupo Toyota Tsusho e temos como principal objetivo atender as necessidades dos clientes com rapidez e eficiência.</div>
        </div>

        <div class="tp-caption"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingtop="[0,0,0,0]"
        data-responsive_offset="on"
        data-type="text"
        data-height="none"
        data-whitespace="nowrap"
        data-width="auto"
        data-hoffset="['-10','5','5','5']"
        data-voffset="['140','140','140','150']"
        data-x="['left','left','left','left']"
        data-y="['middle','middle','middle','middle']"
        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
        <a href="about.html" class="theme-btn btn-style-one"><span>Saiba Mais</span></a>
        <a href="content.html" class="theme-btn btn-style-two"><span>Seguros</span></a>
      </div>
    </li>

    <!-- Slide 1 -->
    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">

      <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-2.jpg">

      <div class="tp-caption"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingtop="[0,0,0,0]"
      data-responsive_offset="on"
      data-type="text"
      data-height="none"
      data-whitespace="nowrap"
      data-textalign="center"
      data-width="auto"
      data-hoffset="['0','0','0','0']"
      data-voffset="['-165','-165','-165','-165']"
      data-x="['center','center','center','center']"
      data-y="['middle','middle','middle','middle']"
      data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
      <h4>Toyota Tsusho</h4>
    </div>

    <div class="tp-caption"
    data-paddingbottom="[0,0,0,0]"
    data-paddingleft="[0,0,0,0]"
    data-paddingright="[0,0,0,0]"
    data-paddingtop="[0,0,0,0]"
    data-responsive_offset="on"
    data-type="text"
    data-height="none"
    data-textalign="center"
    data-whitespace="nowrap"
    data-hoffset="['0','0','0','0']"
    data-voffset="['-75','-75','-75','-95']"
    data-x="['center','center','center','center']"
    data-y="['middle','middle','middle','middle']"
    data-frames='[{"delay":0,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
    <h2>Cuidando do seu amanhã</h2>
  </div>

  <div class="tp-caption"
  data-paddingbottom="[0,0,0,0]"
  data-paddingleft="[0,0,0,0]"
  data-paddingright="[0,0,0,0]"
  data-paddingtop="[0,0,0,0]"
  data-responsive_offset="on"
  data-type="text"
  data-height="none"
  data-whitespace="normal"
  data-textalign="center"
  data-width="['600','600','600','500']"
  data-hoffset="['0','0','0','0']"
  data-voffset="['15','15','15','15']"
  data-x="['center','center','center','center']"
  data-y="['middle','middle','middle','middle']"
  data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
  <div class="text">A Toyota Tsusho Corretora de Seguros é uma empresa do Grupo Toyota Tsusho e temos como principal objetivo atender as necessidades dos clientes com rapidez e eficiência.</div>
</div>

<div class="tp-caption"
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-whitespace="nowrap"
data-width="auto"
data-textalign="center"
data-hoffset="['0','0','0','0']"
data-voffset="['140','140','140','160']"
data-x="['center','center','center','center']"
data-y="['middle','middle','middle','middle']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
<a href="about.html" class="theme-btn btn-style-one"><span>Saiba Mais</span></a>
<a href="content.html" class="theme-btn btn-style-two"><span>Seguros</span></a>
</div>
</li>
</ul>
</div>
</div>
</section>
