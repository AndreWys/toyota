<section class="about-us">
  <div class="auto-container">
    <div class="row clearfix">
      <!-- Content Column -->
      <div class="content-column col-lg-6 col-md-12 col-sm-12">
        <div class="inner-column">
          <span class="title">Sobre a Toyota Tsusho</span>
          <h2>Especializada em prover soluções em seguros de alta complexidade</h2>
          <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</div>
          <div class="link-box">
            <a href="/quem-somos" class="theme-btn btn-style-three">Saiba Mais</a>
            <a href="tel:+55 11 3018-3550" class="theme-btn call-btn"><i class="icon la la-phone"></i> +55 11 3018-3550</a>
          </div>
        </div>
      </div>

      <!-- Video Column -->
      <div class="video-column col-lg-6 col-md-12 col-sm-12">
        <div class="inner-column">
          <figure class="image"><img src="images/resource/image-1.jpg" alt=""></figure>
          <div class="link-box"><a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="link" data-fancybox="gallery" data-caption=""><span class="icon la la-play-circle-o"></span></a></div>
        </div>
      </div>
    </div>
  </div>
</section>
