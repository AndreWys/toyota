<section class="why-us-section">
  <div class="auto-container">
    <div class="row clearfix">
      <!-- Content Column -->
      <div class="content-column col-lg-6 col-md-12 col-sm-12">
        <div class="inner-column">
          <div class="sec-title">
            <span class="title">Por Que Toyota Tsusho</span>
            <img class="m-b-20" src="{{asset('images/patrimonio.png')}}" alt="">
            <h2>Há mais de 35 anos cuidando  <br>do seu patrimônio</h2>
            <div class="text">
              Fundada em 1982, a Toyota Tsusho Corretora de Seguros vem se especializando em atender a necessidade de seus clientes junto ao mercado segurador.
              <br>
              Pertencemos ao grupo Toyota Tsusho Corporation e este faz parte do grupo Toyota Motor Corporation, grupo hoje que atua nos mais diversos segmentos.
            </div>
          </div>
        </div>
      </div>

      <!-- Image Column -->
      <div class="content-column col-lg-6 col-md-12 col-sm-12">
        <div class="inner-column">
          <div class="sec-title">
            <span class="title"><br></span>
            <img class="m-b-20" src="{{asset('images/analise.png')}}" alt="">
            <h2>Mapa de riscos baseado  <br>em uma análise Macro</h2>
            <div class="text">
              As empresas estão expostas a diversos riscos. Para a grande maioria deles não é contratado o Seguro.
              <br>
              Isso ocorre pelas empresas não terem acesso à modalidades de seguros mais específicas e/ou modernas, adequadas à atual exposição de risco.
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
</section>
