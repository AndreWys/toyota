<section class="services-section" style="background-image: url(images/background/1.jpg);">
  <div class="auto-container">
    <div class="sec-title light">
      <span class="title">Nossos Serviços</span>
      <h2>O Que nós provemos</h2>
    </div>

    <div class="services-carousel owl-carousel owl-theme">


      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/saude.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</div>
              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-briefcase-medical"></i></span>
              <h3><a href="service-detail.html">Planos de Saúde</a></h3>
            </div>
          </div>
        </div>
      </div>

      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/odonto.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</div>
              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-briefcase-medical"></i></span>
              <h3><a href="service-detail.html">Planos Odontológicos</a></h3>
            </div>
          </div>
        </div>
      </div>


      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/vida.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</div>
              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-briefcase-medical"></i></span>
              <h3><a href="service-detail.html">Seguro de Vida</a></h3>
            </div>
          </div>
        </div>
      </div>

      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/transporte.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Este seguro tem como objetivo dar tranquilidade ao seu transporte, indenizando as perdas e danos sofridos pelos bens ou mercadorias durante viagens aquaviárias, terrestres e/ou aéreas, em percursos nacionais e internacionais.</div>
              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-truck-moving"></i></span>
              <h3><a href="service-detail.html">Seguro de Transportes</a></h3>
            </div>
          </div>
        </div>
      </div>

      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/auto.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Temos parceria com várias seguradoras, realize uma cotação conosco e confira e melhor opção para a sua necessidade.</div>

              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-car"></i></span>
              <h3><a href="service-detail.html">Seguros de Automóvel</a></h3>
            </div>
          </div>
        </div>
      </div>

      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/empresarial.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Proteger a empresa contra perdas e danos causados às instalções da empresa causados por diversos eventos.</div>
              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-home"></i></span>
              <h3><a href="service-detail.html">Seguros Corporativos</a></h3>
            </div>
          </div>
        </div>
      </div>

      <!-- Service Block -->
      <div class="service-block">
        <div class="inner-box">
          <div class="image-box">
            <figure><img src="{{asset('images/services/house.jpg')}}" alt=""></figure>
            <div class="overlay-box">
              <div class="text">Proteger a empresa contra perdas e danos causados às instalções da empresa causados por diversos eventos.</div>
              <div class="link-box"><a href="service-detail.html">Saiba Mais <i class="la la-angle-double-right"></i></a></div>
            </div>
          </div>
          <div class="caption-box">
            <div class="caption">
              <span class="icon"><i class="fas fa-home"></i></span>
              <h3><a href="service-detail.html">Seguros Massificados</a></h3>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
