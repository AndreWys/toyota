<section class="call-to-action" style="background-image: url({{asset('images/background/para_voce.jpg')}});">
    <div class="auto-container">
      <div class="outer-box clearfix">
        <div class="title-column">
          <h3>Entre em contato conosco e solicite uma cotação!</h3>
        </div>

        <div class="btn-column">
          <div class="btn-box">
            <a href="/contato" class="theme-btn btn-style-one">Fazer Cotação</a>
          </div>
        </div>
      </div>
    </div>
  </section>
