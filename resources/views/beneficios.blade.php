@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/14.jpg);">
        <div class="auto-container">
            <h1>Benefícios <br>
                Saúde, auto-estima e cuidado com você</h1>
                {{-- <ul class="bread-crumb clearfix">
                <li><a href="index.html">Home </li>
                <li>About Us</li>
            </ul> --}}
        </div>
    </section>
    <!--End Page Title-->
    <!-- About Us -->
    <section class="about-section-two alternate">
        <div class="auto-container">
            <div class="row clearfix">
                <!-- Content Column -->
                <div class="content-column col-lg-12 col-md-12 col-sm-12">
                    <div class="inner-column text-center">
                        <span class="title">Ramo: Saúde e Odontológico</span>
                        <span class="title">Finalidade: Benefício oferecido aos colaboradores que tem como finalidade proporcionar o atendimento médico privado.</span>
                        <div class="text">
                            <img src="{{asset('images/beneficios/1.jpg')}}" alt="">
                        </div>

                    </div>
                </div>

                <!-- Content Column -->
                <div class="content-column col-lg-12 col-md-12 col-sm-12">
                    <div class="inner-column text-center">
                        <div class="text">
                            <img src="{{asset('images/beneficios/2.jpg')}}" alt="">
                        </div>
                        <div class="">
                            <h4>CONTRATAR UM SEGURO DE SAÚDE É CADA VEZ MAIS UMA NECESSIDADE PARA OS CIDADÃOS, PRINCIPALMENTE PELO FATO DOS HOSPITAIS DA REDE PÚBLICA NÃO PODEREM OFERECER UM ATENDIMENTO DE QUALIDADE.</h4>
                            <p>Tendo ciência dessa necessidade, atualmente cerca de 75% das empresas oferecem o Seguro Saúde como benefício visando a atração e a retenção de colaboradores por trazer mais segurança e proteção aos associados e seus familiares.</p>
                        </div>
                        <div class="text">
                            <img src="{{asset('images/beneficios/3.jpg')}}" alt="">
                        </div>
                        <div class="text">
                            <img src="{{asset('images/beneficios/4.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
                <!-- Content Column -->
                <div class="content-column col-lg-12 col-md-12 col-sm-12">
                    <div class="inner-column text-center">
                        <span class="title">Ramo: Vida em Grupo</span>
                        <span class="title">Finalidade: Benefício oferecido aos colaboradores que tem como finalidade amparar a família em caso de morte desse funcionário, independente da causa (natural,  acidental) seja durante o trabalho ou não.</span>
                        <div class="text">
                            <img src="{{asset('images/beneficios/5.jpg')}}" alt="">
                        </div>
                        <div class="text">
                            <img src="{{asset('images/beneficios/6.jpg')}}" alt="">
                        </div>
                        <div class="text">
                            <img src="{{asset('images/beneficios/7.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->
    @component('component.faca_cotacao')

    @endcomponent


@endsection
