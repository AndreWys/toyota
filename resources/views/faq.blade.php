@extends('layouts.base')
@section('section')
      <!--Page Title-->
      <section class="page-title parallax" style="background-image:url(images/background/10.jpg);">
            <div class="auto-container">
                  <h1>FAQ <br>
                  </div>
            </section>
            <!--End Page Title-->
            <!-- About Us -->
            <section class="about-section-two alternate">
                  <div class="auto-container">
                        <div class="row clearfix">
                              <!-- Content Column -->
                              <div class="content-column col-lg-12 col-md-12 col-sm-12">
                                    <div class="inner-column">
                                          <h2 class="text-center">PERGUNTAS MAIS FREQUENTES SOBRE SEGUROS</h2>
                                          <div id="accordion">
                                                <div class="card">
                                                      <div class="card-header" id="headingOne">
                                                            <h5 class="mb-0">
                                                                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                        1- As condições contratuais podem ser alteradas após a emissão da apólice?
                                                                  </button>
                                                            </h5>
                                                      </div>

                                                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                            <div class="card-body">
                                                                  Podem. Mas, como qualquer alteração contratual, dependerá de comum acordo entre as partes (segurado e seguradora). No caso de seguros coletivos, as alterações dependem da anuência expressa de 3/4 do grupo interessado.
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="card">
                                                      <div class="card-header" id="headingTwo">
                                                            <h5 class="mb-0">
                                                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                        2- O que se entende por perda de direito?
                                                                  </button>
                                                            </h5>
                                                      </div>
                                                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                            <div class="card-body">
                                                                  Trata-se da ocorrência de um fato que provoca a perda do direito do segurado à indenização, ainda que, a princípio, o sinistro seja oriundo de um risco coberto, ficando, então, a seguradora isenta de qualquer obrigação decorrente do contrato. <br>
                                                                  Ocorre a perda de direito se:
                                                                  <ul>
                                                                        <li>- o sinistro ocorrer por culpa grave ou dolo do segurado ou beneficiário do seguro;</li>
                                                                        <li>- a reclamação de indenização por sinistro for fraudulenta ou de má-fé;</li>
                                                                        <li>- o segurado, corretor, beneficiários ou ainda seus representantes e prepostos fizerem declarações falsas ou, por qualquer meio, tentarem obter benefícios ilícitos do seguro;</li>
                                                                        <li>- o segurado agravar intencionalmente o risco.</li>
                                                                  </ul>
                                                                  Além disso, se o segurado, seu representante, ou seu corretor de seguros fizer declarações inexatas ou omitir circunstâncias que possam influir na aceitação da proposta ou no valor do prêmio, ficará prejudicado o direito à indenização, além de estar o segurado obrigado ao pagamento do prêmio vencido.
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="card">
                                                      <div class="card-header" id="headingThree">
                                                            <h5 class="mb-0">
                                                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                        3- O que é prêmio do seguro?
                                                                  </button>
                                                            </h5>
                                                      </div>
                                                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                            <div class="card-body">
                                                                  É o valor que o segurado paga à seguradora pelo seguro para transferir a ela o risco previsto nas Condições Contratuais. Pagar o prêmio é uma das principais obrigações do segurado.
                                                            </div>
                                                      </div>
                                                </div>

                                                <div class="card">
                                                      <div class="card-header" id="headingFour">
                                                            <h5 class="mb-0">
                                                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                                        4- O que acontece se houver atraso nos pagamentos dos prêmios?
                                                                  </button>
                                                            </h5>
                                                      </div>
                                                      <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                            <div class="card-body">
                                                                  O não pagamento do prêmio nas datas previstas poderá acarretar a suspensão ou até mesmo o cancelamento do seguro, prejudicando o direito à indenização, caso o sinistro ocorra após a data de suspensão ou cancelamento. As condições gerais, na cláusula “pagamento de prêmio”, deverão informar em que hipóteses ocorrerão a suspensão e/ou o cancelamento do contrato em razão da falta de pagamento de prêmio.
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="card">
                                                      <div class="card-header" id="headingFive">
                                                            <h5 class="mb-0">
                                                                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                                        5- A seguradora poderá recusar a proposta?
                                                                  </button>
                                                            </h5>
                                                      </div>
                                                      <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                            <div class="card-body">
                                                                  Sim. A sociedade seguradora tem o prazo de 15 dias para se pronunciar quanto à proposta de seguro, seja para seguros novos ou renovações, bem como para alterações que impliquem modificação do risco, apresentada pelo segurado ou seu corretor. Encerrado este prazo, não tendo havido a recusa da seguradora, o seguro passa a ser considerado aceito. <br>
                                                                  No caso de recusa, a seguradora deverá comunicar formalmente ao segurado a não aceitação do seguro, justificando a recusa.
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                          <div class="card">
                                                <div class="card-header" id="headingSix">
                                                      <h5 class="mb-0">
                                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                                  6- Qual o prazo para receber a indenização?
                                                            </button>
                                                      </h5>
                                                </div>
                                                <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                      <div class="card-body">
                                                            A liquidação dos sinistros deverá ser feita num prazo não superior a 30 dias, contados a partir da entrega de todos os documentos básicos apresentados pelo segurado ou beneficiário(s). <br>
                                                            A contagem do prazo poderá ser suspensa quando, no caso de dúvida fundada e justificável, forem solicitados novos documentos, voltando a correr a partir do dia útil subseqüente àquele em que forem completamente atendidas as exigências pelo segurado ou beneficiário. <br>
                                                            É essencial que o segurado ou beneficiário solicite à sociedade seguradora o devido protocolo que identifique a data do recebimento do aviso de sinistro e respectivos documentos.
                                                      </div>
                                                </div>
                                          </div>
                                          <br><br>
                                          Fonte: susep.gov.br

                                    </div>
                              </div>
                        </div>

                  </div>
            </div>
      </section>
      <!-- End About Us -->

@endsection
