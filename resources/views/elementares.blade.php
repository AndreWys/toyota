@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/15.jpg);">
        <div class="auto-container">
            <h1>Ramos Elementares</h1>
            {{-- <ul class="bread-crumb clearfix">
            <li><a href="index.html">Home </li>
            <li>About Us</li>
        </ul> --}}
    </div>
</section>
<!--End Page Title-->
<!-- About Us -->
<section class="about-section-two alternate">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Property / Patrimonial / Residencial </span>
                    <div class="text text-justify">
                        Finalidade: Proteger a empresa contra perdas e danos causados às instalções da empresa causados por diversos eventos:
                        <ul>
                            <li>Incêndio, queda de raio e explosão;</li>
                            <li>Vendaval;</li>
                            <li>Roubo;</li>
                            <li>Danos elétricos;</li>
                            <li>Etc...</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Lucros Cessantes </span>
                    <div class="text text-justify">
                        Finalidade: Garantir ao segurado a manutenção dos compromissos financeiros durante o período em que houver a paralização em consequencia de um sinistro coberto pela apólice.
                        <br>
                        Poderá ser contratada a reposição dos seguintes itens: Despesas Fixas e Lucro Líquido.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Responsabilidade Civil Geral </span>
                    <div class="text text-justify">
                        Finalidade: Protegerá a empresa contra possíveis condenações por danos causados a terceiros.
                        <br>
                        São inúmeras as possibilidades de danos a terceiros e, por esse motivo aconselhamos a contratação de uma apólice “moderna” que cobre “todos*” os danos causados a terceiros (All Risks).
                        <br>
                        * Exceto os  eventos  excluídos  de forma explícitana apólice
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Responsabilidade Civil - Produtos </span>
                    <div class="text text-justify">
                        Finalidade: Por maior que seja o controle de qualidade, falhas e defeitos no produto podem ocorrer.
                        <br>
                        Esses “defeitos” podem causar diversos danos ao consumidor que poderá haver a responsabilização do Fabricante, Distribuidor, Comerciante, importador, etc... e a apólice de Responsabilidade Civil – Produtos suportará essas reclamações.
                        <br>
                        * Exceto os  eventos  excluídos  de forma explícitana apólice
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Recall </span>
                    <div class="text text-justify">
                        Finalidade: Em casos de identificação de algum potencial “defeito”  que possa causar danos ao consumidor, a empresa é obrigada a retirar o produto do mercado. Essas despesas poderão ser cobertas pela cobertura de Recall.
                        <br>
                        Despesas cobertas:
                        <ul>
                            <li>Anúncio em mídias de grande circulação;</li>
                            <li>Transporte dos produtos defeituosos;</li>
                            <li>Armazenamento dos produtos defeituosos.</li>
                        </ul>
                        <br>
                        * Exceto os  eventos  excluídos  de forma explícitana apólice
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Responsabilidade Civil - Empregador </span>
                    <div class="text text-justify">
                        Finalidade: Proteger a empresa contra possíveis responsabilizações por acidentes de traabalho que tiverem como consequência a morte ou invalidez permanente do funcionário.
                        <br>
                        Importante informar que esse seguro cobrirá também a ida e volta do trabalho, desde que em veículo alugado/contratado pela empresa.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Responsabilidade Civil -  Diretores e Executivos (D&O) </span>
                    <div class="text text-justify">
                        Finalidade: O Executivo pode responder pessoalmente por falhas e condenações durante a gestão.
                        <br>
                        Esse risco aumentou expressivamente após o Novo Código Civil que, de forma explícita, responsabiliza o executivo, que poderá comprometer o patrimônio pessoal.
                        <br>
                        Alguns principais riscos:
                        <ul>
                            <li>Reclamações Trabalhistas;</li>
                            <li>Danos Ambientais;</li>
                            <li>Fisco;</li>
                            <li>Órgãos Reguladores;</li>
                            <li>Etc...</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Responsabilidade Civil – Profissional (E&O) </span>
                    <div class="text text-justify">
                        Finalidade: Alguns setores da economia, principalemte as empresas prestadoras de serviços podem ser  responsabilizadas por falhas inerentes à atividade.
                        <br>
                        O Seguro de Responsabilidade Civil –Profissional tem como finalidadeproteger o segurado contra essas  possíveis condenações.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Responsabilidade Civil - Danos Ambientais </span>
                    <div class="text text-justify">
                        Finalidade: Todas as empresas estão passíveis de causarem danos ambientais.
                        <br>
                        Esta apólice protegerá a empresa por possíveis responsabilizações por esses danos.
                        <br>
                        Algumas situações  críticas:
                        <ul>
                            <li>Transporte;</li>
                            <li>Incêndio/ Explosão;</li>
                            <li>Armazenamento de substâncias líquidas;</li>
                            <li>Etc...</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro - Sequestro </span>
                    <div class="text text-justify">
                        Finalidade: Proteger o patrimônio dos executivos em casos de Sequestros.
                        <br>
                        Esse benefício poderá se estender para os familiares e empregados que trabalham na residência.
                        <br>
                        Principais coberturas:
                        <ul>
                            <li>Pagamento do resgate;</li>
                            <li>Contratação de negociadores profissionais;</li>
                            <li>Apois psicológico após a ocorrência;</li>
                            <li>Cirurgias plásticas reparadoras;</li>
                            <li>Etc...</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro Garantia - Judicial </span>
                    <div class="text text-justify">
                        Finalidade: Substituir as garantias tradicionalmente apresentadas nos depósitos judiciais (fiança bancária, penhor, etc...) por uma apólice de seguros que, além de possuir um custo infinitamente menor, disponibiliza ativos para o capital de giro da empresa (Aplicações Financeiras, Bens,Imóveis, etc...).
                        <br>
                        Vale informar que as garantias apresentadas  anteriormente poderão ser substituídas pela apólice.
                        <br>
                        Esse movimento é bastante benéfico nesses momentos de crise diante do alto custo de captação de recursos de terceiros.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro Garantia - Administrativa </span>
                    <div class="text text-justify">
                        As empresas com endereço fiscal no Estado de São Paulo, que acumulam ICMS, em especial  as Expostadoras podem antecipar o crédito de forma praticamente imediata contra a apresentação do Seguro Garantia.
                        <br>
                        Essa é uma possibilidade de antecipar recursos sem precisar aguardar o longo prazo de avaliação do  Governo.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro Garantia - Pagamento de Energia </span>
                    <div class="text text-justify">
                        Finalidade: No mercado aberto de comercialização de energia, as empresas compradoras precisam apresentar uma garantia de pagamento.
                        <br>
                        O Seguro Garantia é plenamente aceita para essa finalidade.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro Garantia - Performance Bond </span>
                    <div class="text text-justify">
                        Finalidade: Garantir ao seu cliente a entrega do objeto do contrato.
                        <br>
                        Para as empresas públicas é uma exigência em todos os contratos de fornecimento (produtos e serviços) – Lei 8.666
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro de Crédito </span>
                    <div class="text text-justify">
                        Finalidade: Proteger a empresa por possíveis indadimplências de vendas realizada a prazo.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Riscos de Engenharia </span>
                    <div class="text text-justify">
                        Finalidade: Todo projeto de engenharia traz em si uma série de riscos. Com esta modalidade de seguro, o projeto em execução, estará coberto contra os riscos de: acidentes, incêndio, explosão, roubo, furto, eventos da natureza, etc...
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Afinidades </span>
                    <div class="text text-justify">
                        Finalidade: Proporcionar aos colaboradores e familiares* condições diferenciadas na contratação dos seguros pessoais.
                        <br>
                        <ul>
                            <li>Descontos;</li>
                            <li>Prazo de pagamento;;</li>
                            <li>Etc...</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Fiança Locatícia </span>
                    <div class="text text-justify">
                        Finalidade: Na locação de imóveis, substituir o fiador por uma apólice, garantindo o pagamento do aluguel e outras despesas.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <span class="title">Ramo: Seguro Viagem </span>
                    <div class="text text-justify">
                        Finalidade: Seguro Viagem Negócios foi pensado para cobrir as necessidades e garantir a tranquilidade de executivos que viajam a trabalho.
                        <br>
                        Faz viagens constantes para o exterior ou para diversos estados brasileiros a trabalho? Não corra riscos desnecessários! Com o Seguro Viagem Negócios você terá uma série de benefícios*, como Assistência Médica, Localização de bagagem, Substituição de Executivo em caso de necessidade, Seguro Acidentes, Concierge, entre outros. O atendimento é totalmente em português e 24 horas por dia.
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->
    <!-- Call To Action -->
    @component('component.faca_cotacao')

    @endcomponent

@endsection
