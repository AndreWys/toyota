@extends('layouts.base')
@section('section')
  <!--Page Title-->
  <section class="page-title" style="background-image:url(images/background/8.jpg);">
    <div class="auto-container">
      <h1>Contato</h1>
    </div>
  </section>
  <!--End Page Title-->

  <!-- Contact Form Section -->
  <section class="contact-form-section">
    <div class="auto-container">
      <div class="inner-container">
        <div class="sec-title text-center">
          <h2 class="title">Deixei sua Mensagem</h2>
        </div>
        <div class="contact-form-two">
          <form method="post" id="contactForm">
            {{ csrf_field() }}
            <input type="hidden" value="{{date('d/m/Y')}}" name="data">
            <div class="row clearfix">
              <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                <input type="text" name="nome" placeholder="Nome" required="">
              </div>

              <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                <input type="email" name="email" placeholder="Email" required="">
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <input type="text" name="assunto" placeholder="Assunto" required="">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                <textarea name="mensagem" placeholder="Sua Mensagem"></textarea>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center">
                <button class="theme-btn btn-style-three btnContato">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- End Contact Form Section -->

  <!-- Contact Info Section -->
  <section class="contact-info-section">
    <div class="auto-container">
      <div class="row clearfix">
        <div class="contact-info-box col-lg-3 col-md-6 col-sm-12">
          <div class="inner-box">
            <div class="icon-box"><span class="flaticon-mail"></span></div>
            <ul>
              <li><a   href="#">toyota_seguros@toyotatsusho.com.br</a></li>
              <li>+55 11 3018-3550</li>
            </ul>
          </div>
        </div>

        <div class="contact-info-box col-lg-3 col-md-6 col-sm-12">
          <div class="inner-box">
            <div class="icon-box"><span class="flaticon-watch-2"></span></div>
            <ul>
              <li>Seg à sex - 8:30h às 17:30h</li>
            </ul>
          </div>
        </div>

        <div class="contact-info-box col-lg-3 col-md-6 col-sm-12">
          <div class="inner-box">
            <div class="icon-box"><span class="flaticon-pin-1"></span></div>
            <ul>
              <li><strong>Matriz</strong></li>
              <li style="font-size: 13px;">
                Alameda Campinas, 977, <br>
                Jardim Paulista - São Paulo - SP
            </li>
            </ul>
          </div>
        </div>

        <div class="contact-info-box col-lg-3 col-md-6 col-sm-12">
          <div class="inner-box">
            <div class="icon-box"><span class="flaticon-pin-1"></span></div>
            <ul>
              <li><strong>Filial </strong></li>
              <li style="font-size: 13px;">
                Av. Antônio Carlos Comitre, 540 - SL 55 ,<br>
                Parque Campolim, Sorocaba - SP
            </li>
            </ul>
          </div>
        </div>


      </div>
    </div>
  </section>
  <!-- End Contact Info Section -->

  <!-- Contact Map Section -->
  <section class="contact-map-section">
    <div class="row">
      <div class="col-lg-6 col-md-6 com-sm-12">
        <div class="map-outer">
          <div class="map-canvas"
          data-zoom="12"
          data-lat="-37.817085"
          data-lng="144.955631"
          data-type="roadmap"
          data-hue="#ffc400"
          data-title="Envato"
          data-icon-path="images/icons/map-marker-2.png"
          data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
        </div>
      </div>
    </div>

    <div class="col-lg-6 col-md-6 com-sm-12">
      <div class="map-outer">
        <div class="map-canvas"
        data-zoom="12"
        data-lat="-37.817085"
        data-lng="144.955631"
        data-type="roadmap"
        data-hue="#ffc400"
        data-title="Envato"
        data-icon-path="images/icons/map-marker-2.png"
        data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
      </div>
    </div>
  </div>
</div>

</section>
<!--End Contact Map Section -->

@endsection
@section('extra_JS')
  <!--Google Map APi Key-->
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
  <script src="js/map-script.js"></script>
  <!--End Google Map APi-->

@endsection
