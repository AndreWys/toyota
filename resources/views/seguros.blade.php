@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/11.jpg);">
        <div class="auto-container">
            <h1>Seguros</h1>
            {{-- <ul class="bread-crumb clearfix">
            <li><a href="index.html">Home </li>
            <li>About Us</li>
        </ul> --}}
    </div>
</section>
<!--End Page Title-->
<!-- About Us -->
<section class="about-section-two alternate">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <h2>Ramos de Seguros da Toyota Tsusho Corretora de Seguros</h2>
                    <div class="row">
                        <div class="col-lg-4">
                            <img src="{{asset('images/patrimoniais.png')}}" alt="">
                            <p>Patrimônio (Fábricas, equipamentos, armazéns, escritórios).</p>
                            <p>Construção.</p>
                            <p>Transportes (importação & exportação).</p>
                        </div>
                        <div class="col-lg-4">
                            <img src="{{asset('images/plano_de_sucesso.jpg')}}" alt="">
                        </div>
                        <div class="col-lg-4">
                            <img src="{{asset('images/responsabilidade_civil.png')}}" alt="">
                            <p>Crédito e garantia</p>
                            <p>Fiança locatícia</p>
                        </div>
                        <div class="col-lg-4 ">
                            <img src="{{asset('images/riscos.png')}}" alt="">
                            <p>Automóvel</p>
                            <p>RC (PL, construção e etc.)</p>
                            <p>D&O</p>
                        </div>
                        <div class="col-lg-4">
                            <img src="{{asset('images/casa.jpg')}}" alt="">
                        </div>
                        <div class="col-lg-4">
                            <img src="{{asset('images/seguros_colaboladores.png')}}" alt="">
                            <p>Saúde</p>
                            <p>Vida</p>
                            <p>Odonto</p>
                            <p>Residencial</p>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- About Us -->
<section class="about-section-two alternate">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <h2>Nossos serviços</h2>
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="{{asset('images/1.png')}}" alt="">
                        </div>
                        <div class="col-lg-10 vertical-center">
                            <p>Grande experiência e expertise nos ramos trabalhados, obtendo melhores condições e parcerias com as seguradoras e oferecendo melhor custo benefício para o perfil do cliente (cobertura x prêmio).</p>
                        </div>
                        <div class="col-lg-2 ">
                            <img src="{{asset('images/2.png')}}" alt="">
                        </div>
                        <div class="col-lg-10 vertical-center" >
                            <p>No momento do sinistro, rápido atendimento para solução do problema e devido pagamento do sinistro.</p>
                        </div>
                        <div class="col-lg-2">
                            <img src="{{asset('images/3.png')}}" alt="">
                        </div>
                        <div class="col-lg-10 vertical-center">
                            <p>Apresentarmos no mínimo duas cotações por seguro.</p>
                            <p>Apresentarmos informações e tendências de seguros das empresas japonesas.</p>
                        </div>
                        <div class="col-lg-2">
                            <img src="{{asset('images/4.png')}}" alt="">
                        </div>
                        <div class="col-lg-10 vertical-center">
                            <p>Rápido atendimento também em japonês sobre o ramo contratado e em caso de sinistro de automóvel.</p>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection
