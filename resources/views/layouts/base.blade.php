<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Toyota Tsusho Corretora de Seguros</title>
  <!-- Stylesheets -->
  <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
  <link href="{{asset('plugins/revolution/css/settings.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
  <link href="{{asset('plugins/revolution/css/layers.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
  <link href="{{asset('plugins/revolution/css/navigation.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
  <link href="{{asset('css/custom.css')}}" rel="stylesheet">
  <!--Color Themes-->
  <link id="theme-color-file" href="{{asset('css/color-themes/default-theme.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">
  <!-- Responsive -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

  <div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header-->
    <header class="main-header">

      <!--Header Top-->
      <div class="header-top">
        <div class="auto-container">
          <div class="inner-container clearfix">
            <div class="top-left">
              <ul class="contact-list clearfix">
                <li><i class="la la-phone-square"></i> <a href="tel:+55 11 3018-3550">+55 11 3018-3550</a></li>
                <li><i class="la la-envelope-o"></i> <a href="mailto:toyota_seguros@toyotatsusho.com.br">toyota_seguros@toyotatsusho.com.br</a></li>
                <li><i class="fas fa-clock"></i>Segunda à sexta - 8:30h às 17:30h</li>
              </ul>
            </div>
            <div class="top-right clearfix">
              <ul class="social-icon-one">
                <li><a target="_blank" href="https://www.facebook.com/toyotatsushocorretora"><i class="fab fa-facebook-square"></i></a></li>
                <li><a target="_blank" href="http://linkedin.com/toyotatsusho"><i class="fab fa-linkedin"></i></a></li>
                <li><a class="acesso" href="#"><i class="fas fa-lock"></i>&nbsp;&nbsp; <strong> Acesso exclusivo do cliente </strong></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- End Header Top -->

      <!-- Header Lower -->
      <div class="header-lower">
        <div class="auto-container">
          <div class="inner-container clearfix">
            <div class="logo-box">
              <div class="logo"><a href="index.html"><img src="images/logo.png" alt="" title=""></a></div>
            </div>

            <div class="nav-outer">
              <div class="main-box clearfix">
                <!-- Main Menu -->
                <nav class="main-menu navbar-expand-md navbar-light">
                  <div class="navbar-header">
                    <!-- Toggle Button -->
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="la la-bars"></span>
                    </button>
                  </div>

                  <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                    <ul class="navigation clearfix">
                      <li class="{{ Request::path() == '/' ? 'current' : ''}}"><a href="/">Home</a></li>
                      <li class="dropdown {{ Request::path() == 'quem-somos' || Request::path() == 'meio-ambiente' ? 'current' : ''}}"><a href="#">Empresa</a>
                        <ul>
                          <li><a href="/quem-somos">Quem Somos</a></li>
                          <li><a href="/meio-ambiente">Meio Ambiente</a></li>
                        </ul>
                      </li>
                      <li class="dropdown {{ Request::path() == 'seguros' || Request::path() == 'auto-frota' || Request::path() == 'beneficios' || Request::path() == 'ramos-elementares' || Request::path() == 'transportes'  || Request::path() == 'cyber-seguro' ? 'current' : ''}}"><a href="#">Seguros</a>
                        <ul>
                          <li><a href="/seguros">Seguros</a></li>
                          <li><a href="/auto-frota">Auto e Frota</a></li>
                          <li><a href="/beneficios">Beneficios</a></li>
                          <li><a href="/ramos-elementares">Ramos Elementares</a></li>
                          <li><a href="/transportes">Transportes</a></li>
                          <li><a href="/cyber-seguro">Cyber Seguro</a></li>
                        </ul>
                      </li>
                      <li class="{{ Request::path() == 'faq' ? 'current' : ''}}"><a href="/faq">FAQ</a></li>
                      <li class="{{ Request::path() == 'noticias' || Request::path() == 'noticias-detalhes' ? 'current' : ''}}"><a href="/noticias">Noticias</a></li>
                      <li class="{{ Request::path() == 'contato' ? 'current' : ''}}"><a href="/contato">Contato</a></li>
                    </ul>
                  </div>
                </nav><!-- Main Menu End-->

                <!-- Main Menu End-->
                <div class="outer-box">
                  <!--Search Box-->
                  <div class="search-box-outer">
                    <div class="dropdown">
                      <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="la la-search"></span></button>
                      <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                        <li class="panel-outer">
                          <div class="form-container">
                            <form method="post" action="blog.html">
                              <div class="form-group">
                                <input type="search" name="field-name" value="" placeholder="Search Here" required>
                                <button type="submit" class="search-btn"><span class="la la-search"></span></button>
                              </div>
                            </form>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--End Header Lower-->

      <!-- Sticky Header -->
      <div class="sticky-header">
        <div class="auto-container clearfix">
          <!--Logo-->
          <div class="logo pull-left">
            <a href="index.html" title=""><img src="images/logo-small.png" alt="" title=""></a>
          </div>

          <!--Right Col-->
          <div class="pull-right">
            <!-- Main Menu -->
            <nav class="main-menu navbar-expand-md navbar-light">
              <div class="navbar-collapse collapse clearfix">
                <ul class="navigation clearfix">
                  <li class="{{ Request::path() == '/' ? 'current' : ''}}"><a href="/">Home</a></li>
                  <li class="dropdown {{ Request::path() == 'quem-somos' || Request::path() == 'meio-ambiente' ? 'current' : ''}}"><a href="#">Empresa</a>
                    <ul>
                      <li><a href="/quem-somos">Quem Somos</a></li>
                      <li><a href="/meio-ambiente">Meio Ambiente</a></li>
                    </ul>
                  </li>
                  <li class="dropdown {{ Request::path() == 'seguros' || Request::path() == 'auto-frota' || Request::path() == 'beneficios' || Request::path() == 'ramos-elementares' || Request::path() == 'transportes'  || Request::path() == 'cyber-seguro' ? 'current' : ''}}"><a href="#">Seguros</a>
                    <ul>
                      <li><a href="/seguros">Seguros</a></li>
                      <li><a href="/auto-frota">Auto e Frota</a></li>
                      <li><a href="/beneficios">Beneficios</a></li>
                      <li><a href="/ramos-elementares">Ramos Elementares</a></li>
                      <li><a href="/transportes">Transportes</a></li>
                      <li><a href="/cyber-seguro">Cyber Seguro</a></li>
                    </ul>
                  </li>
                  <li class="{{ Request::path() == 'faq' ? 'current' : ''}}"><a href="/faq">FAQ</a></li>
                  <li class="{{ Request::path() == 'noticias' || Request::path() == 'noticias-detalhes' ? 'current' : ''}}"><a href="/noticias">Noticias</a></li>
                  <li class="{{ Request::path() == 'contato' ? 'current' : ''}}"><a href="/contato">Contato</a></li>
                </ul>
              </div>
            </nav><!-- Main Menu End-->
          </div>
        </div>
      </div>
    </header>
    <!--End Main Header -->

    @yield('section')

    <!--Main Footer-->
    <footer class="main-footer">
      <!--footer upper-->
      <div class="footer-upper">
        <div class="auto-container">

          <div class="row clearfix">
            <!--Footer Column-->
            <div class="footer-column col-lg-4 col-md-6 col-sm-12">
              <div class="footer-widget about-widget">
                <div class="logo">
                  <a href="index.html"><img src="images/footer-logo.png" alt="" /></a>
                </div>
                {{-- <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</div> --}}
                <ul class="info-list">
                  <li><span class="la la-clock-o"></span> Segunda à sexta - 8:30h às 17:30h</li>
                  <li><span class="la la-home"></span> Matriz <br>
                      Alameda Campinas, 977, <br>
                      Jardim Paulista -  São Paulo / SP</li>
                  <li><span class="la la-home"></span>
                    Filial <br>
                    Av. Antônio Carlos Comitre, 540 - SL 55 , <br>
                    Parque Campolim, Sorocaba / SP</li>
                </ul>
              </div>
            </div>


            <!--Footer Column-->
            <div class="footer-column col-lg-4 col-md-6 col-sm-12">
              <!--Footer Column-->
              <div class="footer-widget recent-posts">
                <h2 class="widget-title">Ultimas Noticias</h2>
                <!--Footer Column-->
                <div class="widget-content">
                  <div class="post">
                    <div class="thumb"><a href="/noticias-detalhes"><img src="{{asset('images/news_footer_1.jpg')}}" alt=""></a></div>
                    <h4><a href="/noticias-detalhes">Cyber Seguro, sua Empresa está protegida?</a></h4>
                    <span class="date">28 Dec, 2017</span>
                  </div>

                  <div class="post">
                    <div class="thumb"><a href="/noticias-detalhes"><img src="{{asset('images/news_footer_2.jpg')}}" alt=""></a></div>
                    <h4><a href="/noticias-detalhes">Agronegócio: Soluções de gestão de risco.</a></h4>
                    <span class="date">13 Oct, 2017</span>
                  </div>
                </div>
              </div>
            </div>


            <!--Footer Column-->
            <div class="footer-column col-lg-4 col-md-6 col-sm-12">
              <div class="footer-widget newsletter-widget">
                <h2 class="widget-title">Newsletter</h2>
                <div class="widget-content">
                  <div class="text">Se Inscreva para receber nossas novidades.</div>
                  <div class="newsletter-form">
                    <form method="post" action="/contato">
                      <div class="form-group">
                        <input type="email" name="email" value="" placeholder="Digite seu Melhor Email" required="">
                        <button type="submit"><i class="la la-paper-plane"></i></button>
                      </div>
                    </form>
                  </div>
                  <div class="social-links">
                    <h4>Siga Nos</h4>
                    <ul class="social-icon-two">
                      <li><a target="_blank" href="https://www.facebook.com/toyotatsushocorretora"><i class="fab fa-facebook-square"></i></a></li>
                      <li><a target="_blank" href="http://linkedin.com/toyotatsusho"><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--Footer Bottom-->
      <div class="footer-bottom">
        <div class="auto-container">
          <div class="inner-container clearfix text-center">
            <div class="copyright-text ">
              <p> &copy; {{date('Y')}} Toyota Tsusho Corretora de Seguros. | Developed By <a target="_blank" href="https://agenciawys.com.br">Agência Wys - Thinking Forward</a></p>
            </div>
            <!--Scroll to top-->
            <div class="scroll-to-top scroll-to-target" data-target="html"><span class="la la-angle-up"></span></div>
          </div>
        </div>
      </div>
    </footer>
    <!--End Main Footer-->
  </div>
  <!--End pagewrapper-->

  <!--Scroll to top-->
  <script src="{{asset('js/jquery.js')}}"></script>
  <script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <!--Revolution Slider-->
  <script src="{{asset('plugins/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
  <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
  <script src="{{asset('js/main-slider-script.js')}}"></script>
  <!--End Revolution Slider-->
  <script src="{{asset('js/jquery.fancybox.js')}}"></script>
  <script src="{{asset('js/owl.js')}}"></script>
  <script src="{{asset('js/wow.js')}}"></script>
  <script src="{{asset('js/appear.js')}}"></script>
  <script src="{{asset('js/script.js')}}"></script>
  <script src="{{asset('js/email.js')}}"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  @yield('extra_JS')
</body>
</html>
