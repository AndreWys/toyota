@extends('layouts.base')
@section('section')
  <!--Page Title-->
  <section class="page-title parallax" style="background-image:url(images/background/17.jpg);">
    <div class="auto-container">
      <h1>Lorem ipsum dolor</h1>
      {{-- <ul class="bread-crumb clearfix">
      <li><a href="index.html">Home </li>
      <li>About Us</li>
    </ul> --}}
  </div>
</section>

<!-- Sidebar Page Container -->
<div class="sidebar-page-container">
  <div class="auto-container">
    <div class="row clearfix">
      <!--Content Side-->
      <div class="content-side col-lg-8 col-md-12 col-sm-12">
        <div class="blog-detail">
          <div class="inner-box">
            <div class="image-box">
              <div class="image"><img src="{{asset('images/detalhes_noticias_1.jpg')}}" alt=""></div>
              <h3>Lorem ipsum dolor sit amet, consectetur adipisicing.</h3>
              <ul class="info">
                <li>Jan.20, 2016</li>
                <li><a href="#">by Adam Smith</a></li>
              </ul>
            </div>
            <div class="content-box sticky-container">
              <!-- Post Share Option -->
              {{-- <div class="post-share-options">
                <div class="social-links sticky-box">
                  <a href="#"><span class="la la-facebook"></span></a>
                  <a href="#"><span class="la la-twitter"></span></a>
                  <a href="#"><span class="la la-google-plus"></span></a>
                  <a href="#"><span class="la la-dribbble"></span></a>
                  <a href="#"><span class="la la-pinterest"></span></a>
                  <h4>Share:</h4>
                </div>
              </div> --}}
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

              <div class="two-column">
                <div class="row clearfix">
                  <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="image"><a href="{{asset('images/detalhes_noticias_2.jpg')}}" class="lightbox-image" data-fancybox="images"><img src="{{asset('images/detalhes_noticias_2.jpg')}}" alt=""></a></div>
                  </div>
                  <div class="text-column col-lg-6 col-md-12 col-sm-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate</p>
                  </div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            </div>
          </div>

          {{-- <div class="post-control">
            <ul class="clearfix">
              <li class="prev pull-left"><a href="#"><i class="la la-angle-double-left"></i> Prev</a></li>
              <li class="next pull-right"><a href="#">Next <i class="la la-angle-double-right"></i></a></li>
            </ul>
          </div> --}}

        </div><!-- Blog List -->
      </div>

      <!--Sidebar Side-->
      <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
        <aside class="sidebar default-sidebar">

          {{-- <!--search box-->
          <div class="sidebar-widget search-box">
            <form method="post" action="blog.html">
              <div class="form-group">
                <input type="search" name="search-field" value="" placeholder="Search....." required="">
                <button type="submit"><span class="icon la la-search"></span></button>
              </div>
            </form>
          </div> --}}

          <!-- Latest News -->
          <div class="sidebar-widget latest-news">
            <div class="sidebar-title"><h2>Ultimas Noticias</h2></div>
            <div class="widget-content">
              <article class="post">
                <div class="post-thumb"><a href="/noticias-detalhes"><img src="{{asset('images/detalhes_noticias_3.jpg')}}" alt=""></a></div>
                <h3><a href="/noticias-detalhes">Cyber Seguro, sua Empresa está protegida?</a></h3>
                <div class="post-info">Dec. 20, 2017</div>
              </article>

              <article class="post">
                <div class="post-thumb"><a href="/noticias-detalhes"><img src="{{asset('images/detalhes_noticias_4.jpg')}}" alt=""></a></div>
                <h3><a href="/noticias-detalhes">Transportes: Gestão entre seguradoras e clientes</a></h3>
                <div class="post-info">Dec. 20, 2017</div>
              </article>

              <article class="post">
                <div class="post-thumb"><a href="/noticias-detalhes"><img src="{{asset('images/detalhes_noticias_5.jpg')}}" alt=""></a></div>
                <h3><a href="/noticias-detalhes">Agronegócio: Soluções de gestão de risco personalizadas</a></h3>
                <div class="post-info">Dec. 20, 2017</div>
              </article>
            </div>
          </div>

          <!-- Categories -->
          <div class="sidebar-widget categories">
            <div class="sidebar-title"><h2>Best Categories</h2></div>
            <ul class="cat-list">
              <li><a href="#">Cyber Seguro <span>(6)</span></a></li>
              <li><a href="#">Transportes <span>(9)</span></a></li>
              <li><a href="#">Agronegócio <span>(3)</span></a></li>
              <li><a href="#">Automoveis <span>(5)</span></a></li>
            </ul>
          </div>



          {{-- <!-- Tags -->
          <div class="sidebar-widget tags">
            <div class="sidebar-title"><h2>Cool Tags</h2></div>
            <ul class="tag-list clearfix">
              <li><a href="#">Design</a></li>
              <li><a href="#">Fashion</a></li>
              <li><a href="#">Money</a></li>
              <li><a href="#">Entertanment</a></li>
              <li><a href="#">Logo</a></li>
              <li><a href="#">Business</a></li>
              <li><a href="#">Development</a></li>
            </ul>
          </div> --}}
        </aside>
      </div>
    </div>
  </div>
</div>
<!-- End Sidebar Container -->



@endsection
