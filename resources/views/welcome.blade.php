@extends('layouts.base')
@section('section')


  <!--Main Slider-->
  @component('component.slider_home')

  @endcomponent
  <!--End Main Slider-->

  <!-- About Us -->
  @component('component.about_home')

  @endcomponent
  <!-- End About Us -->

  <!-- Services Section -->
  @component('component.services_home')

  @endcomponent
  <!-- End Services Section -->

  <!-- Why Choose Us -->
    @component('component.why_home')

    @endcomponent
  <!--End Why Choose Us -->

  <!-- Call To Action -->
  @component('component.solucao_empresarial')

  @endcomponent
  <!--End Call To Action -->

  @component('component.news_home')

  @endcomponent

  <!-- Call To Action -->
  @component('component.solucao_pessoal')

  @endcomponent
  <!--End Call To Action -->
  <!--Clients Section-->
  <section class="clients-section">
    <div class="auto-container">
      <div class="sponsors-outer">
        <!--Sponsors Carousel-->
        <ul class="sponsors-carousel owl-carousel owl-theme">
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/allianz.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/amil.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/azul.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/bradesco.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/liberty.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/mapfre.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/notredame.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/porto.jpg')}}" alt=""></a></figure></li>
          <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('images/parceiros/sulamerica.jpg')}}" alt=""></a></figure></li>
        </ul>
      </div>
    </div>
  </section>
  <!--End Clients Section-->
@endsection
