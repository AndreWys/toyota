@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/8.jpg);">
        <div class="auto-container">
            <h1>Bem vindos a <br>
                Toyota Tsusho</h1>
                {{-- <ul class="bread-crumb clearfix">
                <li><a href="index.html">Home </li>
                <li>About Us</li>
            </ul> --}}
        </div>
    </section>
    <!--End Page Title-->
    <!-- About Us -->
    <section class="about-section-two alternate">
        <div class="auto-container">
            <div class="row clearfix">
                <!-- Content Column -->
                <div class="content-column col-lg-12 col-md-12 col-sm-12">
                    <div class="inner-column text-center">
                        <span class="title">Nós Somos Especilistas</span>
                        <h2>História da Toyota Tsusho Corretora de Seguros</h2>
                        <div class="text text-justify">
                            Fundada em 1982, a Toyota Tsusho Corretora de Seguros vem se especializando em atender a necessidade de seus clientes junto ao mercado segurador.
                            <br>
                            Pertencemos ao grupo Toyota Tsusho Corporation e este faz parte do grupo Toyota Motor Corporation, grupo hoje que atua nos mais diversos segmentos do mercado, não somente se limitando ao ramo automotivo.
                            <br>
                            Nas atividades do grupo Toyota Tsusho e Toyota do Brasil, participamos ativamente nos seguros das operações, atendemos também os fornecedores ligadosd às atividades do grupo e demais empresas do mercado buscando atender de forma clara e objetiva as necessidades securitárias.
                        </div>


                    </div>
                </div>

                <!-- Blocks Column -->
                <div class="block-column col-lg-12 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <div class="row clearfix">
                            <!-- Feature Block -->
                            <div class="feature-block col-lg-4 col-md-4 col-sm-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="image"><img src="images/resource/feature-1.jpg" alt=""></div>
                                    </div>
                                    <div class="lower-content">
                                        <h4>Visão</h4>
                                        <div class="text">
                                            Ser uma Empresa referência no mercado segurador, reconhecida pelo padrão de atendimento aos Clientes, pela qualidade e eficácia dos serviços prestados.
                                            <br>
                                            <br>
                                            Com foco em assessoria, possuímos equipe altamente capacitada para atender nossos clientes, analisar riscos, desenvolver projetos perfeitos e assim conseguirmos as melhores condições do mercado.
                                            <br>
                                            <br>
                                            Mas principalmente amparar e orientar no momento onde mais nos tornamos necessários, no momento do sinistro.
                                            <br>
                                            <br>
                                            Com departamento próprio para acompanhar, solicitar documentos, e tomar as devidas providências para que o impacto deste evento inesperado e desfavorável, seja o menor possível, visando agilizar a indenização.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Feature Block -->
                            <div class="feature-block col-lg-4 col-md-4 col-sm-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="image"><img src="images/resource/feature-2.jpg" alt=""></div>
                                    </div>
                                    <div class="lower-content">
                                        <h4>Missão</h4>
                                        <div class="text">
                                            Melhoria e aprimoramento contínuo, através de treinamentos e qualificação de nossos colaboradores, bem como, interação com o mercado segurador e seus segmentos.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Feature Block -->
                            <div class="feature-block col-lg-4 col-md-4 col-sm-12">
                                <div class="inner-box">
                                    <div class="image-box">
                                        <div class="image"><img src="images/resource/feature-2.jpg" alt=""></div>
                                    </div>
                                    <div class="lower-content">
                                        <h4>Valores</h4>
                                        <div class="text">
                                            Transparências nas relações com os clientes.
                                            <br>
                                            <br>
                                            Comportamento ético.
                                            <br>
                                            <br>
                                            Agilidade no atendimento.
                                            <br>
                                            <br>
                                            Tratamento diferenciado.
                                            <br>
                                            <br>
                                            Incentivar o desenvolvimento dos colaboradores.
                                            <br>
                                            <br>
                                            Transmitir um sentimento de confiança e satisfação.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->
    <!-- Call To Action -->
    @component('component.solucao_empresarial')

    @endcomponent
    <!--End Call To Action -->
    <section class="mapa-about text-center">
        <img src="{{asset('images/mapa.jpg')}}" alt="">
    </section>

@endsection
