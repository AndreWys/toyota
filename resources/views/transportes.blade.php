@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/12.jpg);">
        <div class="auto-container">
            <h1>Seguro de Transportes</h1>
            {{-- <ul class="bread-crumb clearfix">
            <li><a href="index.html">Home </li>
            <li>About Us</li>
        </ul> --}}
    </div>
</section>
<!--End Page Title-->
<!-- About Us -->
<section class="about-section-two alternate">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        Este seguro tem como objetivo dar tranquilidade ao seu transporte, indenizando as perdas e danos sofridos pelos bens ou mercadorias durante viagens aquaviárias, terrestres e/ou aéreas, em percursos nacionais e internacionais.
                        <br>
                        Destina-se principalmente ao proprietário da mercadoria e aos transportadores de cargas.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <h2>Tipos de Seguros</h2>
                    <div class="text">
                        <h4>Seguros para embarcadores (proprietários da carga) </h4>
                        <ul>
                            <li> - Transporte Nacional;</li>
                            <li> - Transporte Internacional - Importação;</li>
                            <li> - Transporte Internacional - Exportação;</li>
                        </ul>
                    </div>
                    <div class="text">
                        <h4>Seguros de Responsabilidade Civil dos Transportadores </h4>
                        <ul>
                            <li> - RCTR-C: Responsabilidade Civil do Transportador Rodoviário de Carga;</li>
                            <li> - RCF-DC: Responsabilidade Civil do Facultativa por Desaparecimento de Carga;</li>
                            <li> - RCTR-VI: Responsabilidade Civil do Transportador Rodoviário de Carga de Viagem Internacional;</li>
                            <li> - RCTA-C: Responsabilidade Civil do Transportador Aéreo de Carga;</li>
                            <li> - RCA-C: Responsabilidade Civil do Armador de Carga;</li>
                            <li> - RCTF-C: Responsabilidade Civil do Transportador Ferroviário de Carga;</li>
                            <li> - RCTI-C: Responsabilidade Civil do Transportador Intermodal de Carga;</li>
                            <li> - RCOTM-C: Responsabilidade Civil do Operador de Transporte Multimodal de Carga;</li>
                        </ul>
                    </div>
                    <div class="text">
                        <h4>Obrigatoriedade da Contratação do Seguro</h4>
                        <ul>
                            <li><strong>Seguro de transporte nacional:</strong> as pessoas jurídicas, de direito público ou privado são obrigadas a segurar os bens/mercadorias de sua propriedade. A obrigatoriedade está disposta no artigo 12 do capítulo VI do Decreto 61.867, de 07/12/67.</li>
                            <li><strong>Seguros de importação e exportação:</strong> a contratação de seguro dessas modalidades não é obrigatória. A modalidade do contrato de compra e venda praticado pelas partes envolvidas determinará a necessidade de contratar o seguro para as mercadorias.</li>
                            <li><strong>Seguro de responsabilidade do transportador:</strong> as pessoas físicas ou jurídicas, de direito público ou privado, que se responsabilizarem pelo transporte de bens/mercadorias de terceiros são obrigadas a contratar <strong>Seguro de Responsabilidade Civil</strong>, como garantia contra as perdas e danos que ocorram às cargas. A obrigatoriedade está disposta no artigo 10 do capítulo IV do Decreto 61.867, de 07/12/67.</li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<!-- End About Us -->

@component('component.faca_cotacao')

@endcomponent

@endsection
