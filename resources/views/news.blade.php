@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/17.jpg);">
        <div class="auto-container">
            <h1>Noticias</h1>
            {{-- <ul class="bread-crumb clearfix">
            <li><a href="index.html">Home </li>
            <li>About Us</li>
        </ul> --}}
    </div>
</section>
<!--End Page Title-->
<!-- About Us -->
<!-- News Section -->
<section class="news-section style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="/noticias-detalhes"><img src="{{asset('images/news_1.jpg')}}" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="/noticias-detalhes">Cyber Seguro, sua Empresa está protegida?</a></h3>
                        <span class="date">Post: 15 Abril 2018</span>
                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
                        <div class="link-box"><a href="/noticias-detalhes">Leia Mais</a></div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="/noticias-detalhes"><img src="{{asset('images/news_2.jpg')}}" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="/noticias-detalhes">Transportes: Gestão entre seguradoras e clientes</a></h3>
                        <span class="date">Post: 15 Abril 2018</span>
                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
                        <div class="link-box"><a href="/noticias-detalhes">Leia Mais</a></div>
                    </div>
                </div>
            </div>

            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <div class="image"><a href="/noticias-detalhes"><img src="{{asset('images/news_3.jpg')}}" alt=""></a></div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="/noticias-detalhes">Agronegócio: Soluções de gestão de risco personalizadas></a></h3>
                        <span class="date">Post: 15 Abril 2018</span>
                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
                        <div class="link-box"><a href="/noticias-detalhes">Leia Mais</a></div>
                    </div>
                </div>
            </div>

        <!--Post Share Options-->
        {{-- <div class="styled-pagination">
            <ul class="clearfix">
                <li class="prev-post"><a href="#"><span class="la la-long-arrow-left"></span> Prev Post</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="active"><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="next-post"><a href="#"> Next Post<span class="la la-long-arrow-right"></span> </a></li>
            </ul>
        </div> --}}
    </div>
</section>
<!-- News Section -->

@endsection
