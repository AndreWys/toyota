@extends('layouts.base')
@section('section')
    <!--Page Title-->
    <section class="page-title parallax" style="background-image:url(images/background/16.jpg);">
        <div class="auto-container">
            <h1>Cyber Seguro</h1>
            {{-- <ul class="bread-crumb clearfix">
            <li><a href="index.html">Home </li>
            <li>About Us</li>
        </ul> --}}
    </div>
</section>
<!--End Page Title-->
<!-- About Us -->
<section class="about-section-two m-b-20 alternate">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <span class="title">Os prejuízos decorrentes de ataques cibernéticos crescem de forma alarmante a cada ano</span>
                    <div class="text text-justify">
                        Cada vez mais, as Empresas estão dependentes da tecnologia para gerenciar seus negócios e informações.
                        <br>
                        Hoje, praticamente todas as empresas trabalham com dados pessoais e corporativos, como número de cartão de crédito, identidade, endereço, registros médicos, passaporte, lista de clientes, orçamento, planos de negócios, planos de marketing etc.
                        <br>
                        Entre as principais indústrias afetadas pela perda de dados, estão: Saúde, Entidades Governamentais, Educação, Instituições Financeiras, Varejo, Telecomunicação, Tecnologia da Informação, Indústrias, ONGs e Prestadores de Serviço.
                    </div>
                </div>
            </div>

            <!-- Blocks Column -->
            <div class="block-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="row clearfix">
                        <!-- Feature Block -->
                        <div class="feature-block col-lg-6 col-md-6 col-sm-12">
                            <div class="lower-content">
                                <div class="text">
                                    Segundo Paula Tudisco, prejuízos decorrentes de crimes virtuais crescem anualmente, com estimativas de prejuízo mundial de US$ 2,1 trilhões até 2019;
                                    <br>
                                    No Brasil, a contratação do serviço ainda esbarra na questão cultural, na falta de informação e na ideia de que esse tipo de seguro é muito caro;
                                </div>
                            </div>
                        </div>

                        <!-- Feature Block -->
                        <div class="feature-block col-lg-4 col-md-4 col-sm-12">
                            <div class="inner-box">
                                <div class="image-box">
                                    <div class="image"><img src="images/cyber.jpg" alt=""></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <span class="title">Os prejuízos decorrentes de ataques cibernéticos crescem de forma alarmante a cada ano</span>
                    <div class="text text-justify">
                        <strong>Responsabilidade por Dados Pessoais e Corporativos:</strong>
                        <br>
                        A divulgação pública de dados privados que estão sob custódia da sociedade e a divulgação pública de dados corporativos de um terceiro (orçamentos, listas de clientes, planos de marketing etc) ou informações profissionais de um terceiro que estejam sob custódia da sociedade e sejam confidenciais.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Responsabilidade pela Segurança de Dados Ato, erro ou omissão que resulte em:</strong>
                        <br>
                        <ul>
                            <li>Contaminação de dados de terceiro por software não autorizado ou código malicioso (vírus);</li>
                            <li>Negação de acesso inadequada para o acesso de um terceiro autorizado aos dados;</li>
                            <li>Roubo ou furto de código de acesso nas instalações da sociedade ou via sistema de computador;</li>
                            <li>Destruição, modificação, corrupção e eliminação de dados armazenados em qualquer sistema de computador;</li>
                            <li>Roubo ou furto físico de hardware da empresa por um terceiro;</li>
                            <li>Divulgação de dados devido a uma violação de segurança de dados.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Responsabilidade por Empresas Terceirizadas</strong>
                        <br>
                        Violação de informação pessoal que resulte em uma reclamação contra uma empresa terceirizada pelo processamento ou coleta de dados pessoais em nome da sociedade e pelos quais a sociedade é responsável.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Custos de Defesa</strong>
                        <br>
                        Honorários advocatícios e custas judiciais incorridos exclusivamente da defesa ou recurso de um procedimento civil, regulatório, administrativo ou criminal.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Investigação</strong>
                        <br>
                        Os honorários, custos e gastos razoáveis que o Segurado incorra para o assessoramento legal e a representação relacionados a uma investigação.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Sanções Administrativas</strong>
                        <br>
                        As Sanções Administrativas que a sociedade seja obrigada a pagar relacionadas a uma investigação.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Restituição de Imagem da Sociedade e Pessoal</strong>
                        <br>
                        Custos e despesas para mitigar os danos à reputação em consequência de uma reclamação coberta pela apólice.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Notificação & Monitoramento</strong>
                        <br>
                        Custos incorridos para a notificação de uma violação de dados aos usuários.
                    </div>
                </div>
            </div>

            <!-- Content Column -->
            <div class="content-column col-lg-12 col-md-12 col-sm-12">
                <div class="inner-column text-center">
                    <div class="text text-justify">
                        <strong>Dados Eletrônicos</strong>
                        <br>
                        No caso de uma ‘’Violação de Dados de Segurança’’, estão cobertos os custos para determinar se os dados eletrônicos podem ser ou não restaurados, restabelecidos ou recriados, e os custos para restaurar, restabelecer ou recriar tais dados eletrônicos, quando possível.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Us -->
<!-- Call To Action -->
@component('component.faca_cotacao')

@endcomponent

@endsection
