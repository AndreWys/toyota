@extends('layouts.base')
@section('section')
      <!--Page Title-->
      <section class="page-title parallax" style="background-image:url(images/background/9.jpg);">
            <div class="auto-container">
                  <h1>Toyota Tsusho <br>
                        Uma Empresa Que Respeita o Meio Ambiente</h1>
                        {{-- <ul class="bread-crumb clearfix">
                        <li><a href="index.html">Home </li>
                        <li>About Us</li>
                  </ul> --}}
            </div>
      </section>
      <!--End Page Title-->
      <!-- About Us -->
      <section class="about-section-two alternate">
            <div class="auto-container">
                  <div class="row clearfix">
                        <!-- Content Column -->
                        <div class="content-column col-lg-12 col-md-12 col-sm-12">
                              <div class="inner-column text-center">
                                    <h2>Responsabilidade Ambiental</h2>
                                    <div class="text text-justify">
                                          O Grupo Toyota Tsusho do Brasil, com unidades de negócios em diversas áreas de atuação, orientado por seus princípios e como boa empresa cidadã, está continuamente comprometido em preservar a segurança e saúde das pessoas e respeitar o meio ambiente.
                                    </div>
                                    <div class="row">
                                          <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="text text-justify">
                                                      A Toyota Tsusho Corretora de Seguros está em constante busca por soluções e alternativas ambientais que visem o bem estar social e ambiental. A empresa é certificada pelo ISO 14001 desde 2008.
                                                </div>
                                          </div>
                                          <div class="col-lg-6 col-md-6 col-sm-12">
                                                <img src="{{asset('images/arvore.png')}}" alt="">
                                          </div>
                                    </div>
                                    
                                    <span class="title">Política de Segurança, Saúde e Meio Ambiente:</span>
                                    <div class="text text-justify">
                                          <ul>
                                                <li>1 - Promover atividades para a preservação da segurança e saúde dos colaboradores e do meio ambiente, através do diálogo e a participação ativa de todos os colaboradores, da economia de energia e da reciclagem, assim como da prevenção da poluição ambiental.</li>
                                                <li>2 - Contribuir para o desenvolvimento de uma sociedade sustentável, com a colaboração de nossos clientes e fornecedores, promovendo ativamente o controle dos riscos à segurança e saúde dos colaboradores e os negócios relacionados à preservação dos recursos naturais e o gerenciamento efetivo dos resíduos gerados, visando a redução, reutilização e reciclagem dos materiais.</li>
                                                <li>3 - Respeitar a legislação aplicável à segurança e saúde e ao meio ambiente e outros requisitos aplicáveis aos negócios.</li>
                                                <li>4 - Construir um sistema de gestão que implemente atividades para promover a segurança e saúde dos colaboradores e a preservação do meio ambiente e avalie seus resultados e promova, com criatividade, sua melhoria continua.</li>
                                                <li>5 - Promover através da educação e do treinamento, a atuação segura, saudável e ambientalmente responsável de todos os colaboradores, zelando assim, pela aplicação efetiva da Política de Meio Ambiente, Saúde e Segurança.</li>
                                          </ul>
                                    </div>



                              </div>
                        </div>

                  </div>
            </div>
      </section>
      <!-- End About Us -->

@endsection
