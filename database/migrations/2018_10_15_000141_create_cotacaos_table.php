<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotacaosTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if (!Schema::hasTable('cotacaos')) {
            Schema::create('cotacaos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome');
                $table->string('telefone');
                $table->string('email');
                $table->string('tipo_seguro');
                $table->string('seg_atual')->nullable();
                $table->string('class_atual')->nullable();
                $table->string('venc_apolice_atual')->nullable();
                $table->string('numero_apolice')->nullable();
                $table->string('modelo_veiculo');
                $table->string('ano_veiculo');
                $table->string('chassis_veiculo');
                $table->string('placa_veiculo');
                $table->string('assunto');
                $table->string('principal_condutor');
                $table->string('cpf_condutor');
                $table->string('cnh_condutor');
                $table->string('data_nascimento_condutor');
                $table->string('estado_condutor');
                $table->string('outros_condutores');
                $table->string('profissao_condutor');
                $table->string('uso_veiculo');
                $table->string('garagem');
                $table->string('tipo_residencia');
                $table->string('endereco');
                $table->string('cep');
                $table->string('tipo_portao');
                $table->string('obs')->nullable();
                $table->string('data');
                $table->timestamps();
            });
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('cotacaos');
    }
}
