<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotacao extends Model
{
  protected $fillable = [
      'nome', 'telefone', 'email', 'tipo_seguro', 'seg_atual', 'class_atual',
      'venc_apolice_atual', 'numero_apolice', 'modelo_veiculo', 'ano_veiculo', 'chassis_veiculo', 'placa_veiculo',
      'assunto', 'principal_condutor', 'cpf_condutor', 'cnh_condutor', 'data_nascimento_condutor', 'estado_condutor',
      'outros_condutores', 'profissao_condutor', 'uso_veiculo', 'garagem', 'tipo_residencia', 'endereco',
      'cep', 'tipo_portao', 'obs', 'data',
  ];
}
