<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Cotacao;

class ContactController extends Controller
{
  public function index()
  {
    return view('contact');
  }
  public function news(Request $request)
  {
    $post = $request->all();
    //dd($post);
    if (
      $request->input('email') != null
    ) {

      News::create($post);

      $emails = ['web.agenciawys@gmail.com'];

      $mail = \Mail::send('mail.news', ['post' => $post], function ($message) use ($emails) {
        $message->to($emails, 'Toyota Tsusho')->subject('Contato Site');
      });

      return response()->json('success');
    } else {
      return response()->json('error');
    }
  }

  public function enviar(Request $request)
  {
    $post = $request->all();
    //dd($post);
    if (
      $request->input('nome') != null &&
      $request->input('email') != null &&
      $request->input('assunto') != null &&
      $request->input('mensagem') != null
    ) {

      Contact::create($post);

      $emails = ['web.agenciawys@gmail.com'];

      $mail = \Mail::send('mail.contact', ['post' => $post], function ($message) use ($emails) {
        $message->to($emails, 'Toyota Tsusho')->subject('Contato Site');
      });

      return response()->json('success');
    } else {
      return response()->json('error');
    }
  }

  public function enviarCotacao(Request $request)
  {
    $post = $request->all();

    if (
      $request->input('nome') != null &&
      $request->input('telefone') != null &&
      $request->input('email') != null &&
      $request->input('tipo_seguro') != null &&
      $request->input('seg_atual') != null &&
      $request->input('modelo_veiculo') != null &&
      $request->input('ano_veiculo') != null &&
      $request->input('chassis_veiculo') != null &&
      $request->input('placa_veiculo') != null &&
      $request->input('assunto') != null &&
      $request->input('principal_condutor') != null &&
      $request->input('cpf_condutor') != null &&
      $request->input('cnh_condutor') != null &&
      $request->input('data_nascimento_condutor') != null &&
      $request->input('estado_condutor') != null &&
      $request->input('outros_condutores') != null &&
      $request->input('profissao_condutor') != null &&
      $request->input('uso_veiculo') != null &&
      $request->input('garagem') != null &&
      $request->input('tipo_residencia') != null &&
      $request->input('endereco') != null &&
      $request->input('cep') != null &&
      $request->input('tipo_portao') != null
    ) {

      Cotacao::create($post);

      $emails = ['web.agenciawys@gmail.com'];

      $mail = \Mail::send('mail.cotacao', ['post' => $post], function ($message) use ($emails) {
        $message->to($emails, 'Toyota Tsusho')->subject('Cotacao Auto Site');
      });

      return response()->json('success');
    } else {
      return response()->json('error');
    }
  }
}
