$('.btnContato').click(function () {
    $('.btnContato').text('Enviando...').attr('disabled', 'disabled');

    var formData = new FormData($('#contactForm')[0]);

    $.ajax({
        url: "enviar",
        method: 'POST',
        dataType: "json",
        processData: false,
        contentType: false,
        data: formData,
        success: function (data) {
            if (data === 'success') {
                $('.btnContato').text('Obrigado!').attr('disabled', 'disabled');
                swal("Enviado!", "Entraremos em contato o mais breve possível!", 'success');
            } else if (data === 'error') {
                swal("Ops!", "Preencha corretamente todos os campos", 'error');
                $('.btnContato').text('Enviar').removeAttr('disabled', 'disabled');
            }
        }
    });
});

$('.btnCotacao').click(function () {
    $('.btnCotacao').text('Enviando...').attr('disabled', 'disabled');

    var formData = new FormData($('#cotacaoForm')[0]);

    $.ajax({
        url: "enviarCotacao",
        method: 'POST',
        dataType: "json",
        processData: false,
        contentType: false,
        data: formData,
        success: function (data) {
            if (data === 'success') {
                $('.btnCotacao').text('Obrigado!').attr('disabled', 'disabled');
                swal("Enviado!", "Entraremos em contato o mais breve possível!", 'success');
            } else if (data === 'error') {
                swal("Ops!", "Preencha corretamente todos os campos", 'error');
                $('.btnCotacao').text('Enviar').removeAttr('disabled', 'disabled');
            }
        }
    });
});
