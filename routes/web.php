<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/quem-somos', function () {
    return view('about');
});
Route::get('/meio-ambiente', function () {
    return view('ambiente');
});
Route::get('/seguros', function () {
    return view('seguros');
});
Route::get('/auto-frota', function () {
    return view('auto');
});
Route::get('/beneficios', function () {
    return view('beneficios');
});
Route::get('/ramos-elementares', function () {
    return view('elementares');
});
Route::get('/transportes', function () {
    return view('transportes');
});
Route::get('/cyber-seguro', function () {
    return view('cyber');
});
Route::get('/faq', function () {
    return view('faq');
});
Route::get('/noticias', function () {
    return view('news');
});
Route::get('/noticias-detalhes', function () {
    return view('news_details');
});

Route::get('/contato', 'ContactController@index');
Route::post('/enviar', 'ContactController@enviar');
Route::post('/enviarCotacao', 'ContactController@enviarCotacao');
